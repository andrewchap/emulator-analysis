#!/bin/bash

mkdir logfiles 2> /dev/null
rsync -ravu pi@10.70.0.102:/home/pi/repos/emulator/build/src/log.txt logfiles/
mv logfiles/log.txt logfiles/IMU-emulator.csv
cp ../dyn-sim/build/src/main/simdata_sw.csv logfiles/
mv logfiles/simdata_sw.csv logfiles/IMU-dyn-sim.csv