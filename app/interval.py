import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly
import plotly.graph_objs as go
from collections import deque
import pandas as pd
import pdb
db = pdb.set_trace


class DataFile:
    def __init__(self, fileName):
        self.fileName = fileName
        self.update_csv()
        self.yvals = dict()

    def update_csv(self):
        # db()
        self.df = pd.read_csv(self.fileName, skipinitialspace=True)
        print('{}: update_csv'.format(self.fileName))

    def get_time(self):
        self.time = list(self.df['sim_time'][1:])
        print('getting time: {}'.format(self.time))
        return self.time

    def get_yval(self, column_name):
        self.yvals[column_name] = list(self.df[column_name][1:])
        return self.yvals[column_name]

    def set_x_range(self):
        if self.time:
            self.xRange[self.time[0], self.time[-1]]
        else:
            self.xRange = [0, 1]


class DataFiles:
    def __init__(self):
        self.filesDict = dict()

    def add_datafile(self, name, fileName):
        self.filesDict[name] = DataFile(fileName)


dataFiles = DataFiles()


#dynSim = DataFile('logfiles/simdata_sw.csv')
#imu = DataFile('logfiles/IMU-emulator.csv')
gps = DataFile('logfiles/GPS.csv')
#dataFiles = [dynSim, imu, gps]

initial_trace = plotly.graph_objs.Scatter(
    x=gps.get_time(),
    y=gps.get_yval('v_GPS0'),
    name='Scatter',
    mode='lines'
)

app = dash.Dash(__name__)
app.layout = html.Div(
    [
        dcc.Checklist(
            id='checklist',
            options=[
                {'label': 'IMU', 'value': 'imu'},
                {'label': 'GPS', 'value': 'gps'},
            ],
            value=['gps']
        ),
        dcc.Graph(id='live-graph',
                  animate=True,
                  figure={'data': [initial_trace]
                          #  'layout': go.Layout(
                          #      xaxis=dict(range=[min(X2), max(X2)]),
                          #      yaxis=dict(range=[min(Y2), max(Y2)]))
                          }),
        dcc.Interval(
            id='graph-update',
            interval=1000
        ),
    ]
)


@ app.callback(
    Output('live-graph', 'figure'),
    [
        Input('graph-update', 'n_intervals'),
        Input('checklist', 'value')
    ]
)
def update_graph_scatter(n, checklist):
    # print('begin update n = {}'.format(n))
    # X.append(X[-1]+1)
    # Y.append(Y[-1]+2)
    gps.update_csv()
    X = gps.get_time()
    Y = gps.get_yval('v_GPS0')
    print('X: {}'.format(X))
    print('Y: {}'.format(Y))
    trace = plotly.graph_objs.Scatter(
        x=X,  # gps.get_time(),
        y=Y,  # gps.get_yval('v_GPS0'),
        name='Scatter',
        mode='lines'
    )
    if X and Y:
        return {'data': [trace],
                'layout': go.Layout(
                xaxis=dict(range=[min(X), max(X)]),
                yaxis=dict(range=[min(Y), max(Y)]))
                }
    else:
        # if we get here we have an empty plot so we can't
        # do min/max, but we still need to return the empty
        # plot so it doesn't hold stale date
        return {'data': [trace]}


if __name__ == '__main__':
    app.run_server(debug=True)
