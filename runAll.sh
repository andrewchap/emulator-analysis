#!/bin/bash


# Start the dash app
pkill -9 -f python3  # kill any previous python app
python3 app/interval.py &

# Start up the emulator
echo 'starting emulator'
ssh pi@emulator4 'pkill emulator' # first kill any other instance of emulator running
ssh pi@emulator4 '/home/pi/repos/emulator/build/src/emulator GPS /home/pi/repos/emulator/build/src/GPS.csv' &

# Start the rsyncing
echo 'rsyncing the results'
while true; do
    rsync -ravu pi@emulator4:/home/pi/repos/emulator/build/src/*.csv logfiles
done